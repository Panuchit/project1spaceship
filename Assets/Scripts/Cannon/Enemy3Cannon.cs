﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Enemy3Cannon : MonoBehaviour
{


    private int HeathEnemy = 200;
    public Text healthTextEnemy;
    public Slider sliderHpEnemy;
    public GameObject enemiesBullet;
    public Transform shootSpawn;
    private double fireRate = 1;
    private float firecounter = 0;


    void Start()
    {

        sliderHpEnemy.maxValue = HeathEnemy;
        sliderHpEnemy.value = HeathEnemy;
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        Enemiesshoot();
        healthTextEnemy.text = "HEALTH: " + HeathEnemy;
        sliderHpEnemy.value = HeathEnemy;

        if (HeathEnemy < 1)
        {
            HeathEnemy = 0;
            Destroy(gameObject);
            

        }
    }


    public void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.tag == "Cannonbullet")
        {
            HeathEnemy = HeathEnemy - 40;
        }

    }
    private void Enemiesshoot()
    {
        firecounter += Time.deltaTime;
        if (firecounter >= fireRate)
        {
            Instantiate(enemiesBullet, shootSpawn.position, shootSpawn.rotation);
            firecounter = 0;
        }

    }
}
