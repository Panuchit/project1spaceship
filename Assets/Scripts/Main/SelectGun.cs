﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SelectGun : MonoBehaviour
{
    public void Cannon ()
    {
        SceneManager.LoadScene("GameCannon");
    }
    public void Laser()
    {
        SceneManager.LoadScene("GameLaser");
    }
}

